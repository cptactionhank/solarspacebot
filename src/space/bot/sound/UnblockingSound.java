package space.bot.sound;

import lejos.nxt.Sound;

public class UnblockingSound extends Thread {

	private int sound;
	
	public UnblockingSound(int sound) {
		this.setDaemon(true);
		this.sound = sound;
	}
	
	public void run() {
		switch (sound) {
			case 1:
				Sound.beep();
				return;
			case 2:
				Sound.buzz();
				return;
			case 3:
				Sound.beepSequenceUp();
				return;
			default :
				Sound.beep();
				return;
		}
	}
	
	public static void sound(int sound) {
		UnblockingSound unblockingSound = new UnblockingSound(sound);
		unblockingSound.start();
	}
	
}
