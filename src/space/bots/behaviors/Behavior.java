package space.bots.behaviors;

import space.bots.arbitration.SpaceBotCommand;

public interface Behavior {

	public void start();

	public SpaceBotCommand requestCommand();
	
	public void won();
	
	public void lost();

}
