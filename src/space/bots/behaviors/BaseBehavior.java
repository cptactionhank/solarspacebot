package space.bots.behaviors;

import space.bots.arbitration.SharedSpaceBot;
import space.bots.arbitration.SpaceBotCommand;

public class BaseBehavior implements Behavior {

	protected SharedSpaceBot bot = new SharedSpaceBot();
	
	@Override
	public void start() {
	}

	@Override
	public SpaceBotCommand requestCommand() {
		return bot.getCommand();
	}

	@Override
	public void won() {
	}
	
	@Override
	public void lost() {
	}

}
