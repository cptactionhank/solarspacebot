package space.bots.behaviors;

import space.bots.arbitration.SharedSpaceBot;
import space.bots.arbitration.SpaceBotCommand;

public class ThreadedBehavior extends Thread implements Behavior {

	protected SharedSpaceBot bot = new SharedSpaceBot();

	public ThreadedBehavior() {
		setDaemon(true);
	}

	@Override
	public String getName() {
		return "Behavior[" + super.getName() + "]";
	}

	@Override
	public SpaceBotCommand requestCommand() {
		return bot.getCommand();
	}

	@Override
	public void won() {
		setPriority(8);
	}
	
	@Override
	public void lost() {
		setPriority(4);
	}

}
