package space.bots;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import space.bots.arbitration.SpaceBotCommand;
import space.bots.arbitration.SpaceMotorCommand;
import space.bots.solar.data.CompetitionState;

public class SpaceBotDriver {

	private final RegulatedMotor rightMotor = new NXTRegulatedMotor(MotorPort.A) { public float getMaxSpeed() { return CompetitionState.SPEED; };};
	private final RegulatedMotor leftMotor = new NXTRegulatedMotor(MotorPort.B) { public float getMaxSpeed() { return CompetitionState.SPEED; };};
	private final DifferentialPilot pilot;

	public SpaceBotDriver() {
		pilot = new DifferentialPilot(5.6, 17.2,
				leftMotor, rightMotor ){
		};
	}
	
	public void perform(SpaceBotCommand cmd) {
		// extract commands
		SpaceMotorCommand motor = cmd.command;
		// control robot motor
		if (motor == SpaceMotorCommand.FORWARD) {
			leftMotor.setSpeed(cmd.leftPower);
			rightMotor.setSpeed(cmd.rightPower);
			leftMotor.forward();
			rightMotor.forward();
		} else if (motor == SpaceMotorCommand.BACKWARD) {
			leftMotor.setSpeed(cmd.leftPower);
			rightMotor.setSpeed(cmd.rightPower);
			leftMotor.backward();
			rightMotor.backward();
		} else if (motor == SpaceMotorCommand.STOP) {
			pilot.stop();
		} else if (motor == SpaceMotorCommand.TURN) {
			pilot.rotate(cmd.turn);
		} else if (motor == SpaceMotorCommand.IGNORE) {
			// noop
		}
	}
}
