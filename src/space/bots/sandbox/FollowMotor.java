package space.bots.sandbox;
import space.bots.arbitration.SharedSpaceBot;
import lejos.nxt.*;
import lejos.util.Delay;

/*
 * Follow behavior , inspired by p. 304 in
 * Jones, Flynn, and Seiger: 
 * "Mobile Robots, Inspiration to Implementation", 
 * Second Edition, 1999.
 */

class FollowMotor extends Thread {
	final int forward = 1, backward = 2, stop = 3;

	private SharedSpaceBot car = new SharedSpaceBot();

	private int power = 70, ms = 750;
	LightSensor light = new LightSensor(SensorPort.S4);
	MotorPort motor = MotorPort.A;

	int frontLight, leftLight, rightLight, delta;
	int lightThreshold;
	int angle = 60;
	int powerHorizontal = 70;

	public FollowMotor(SharedSpaceBot car) {
		this.setDaemon(true);
		this.car = car;
		lightThreshold = light.getLightValue();
		motor.resetTachoCount();
	}

	public void run() {
		while (true) {
			// Monitor the light in front of the car and start to follow
			// the light if light level is above the threshold
			frontLight = light.getLightValue();
			while (frontLight <= lightThreshold) {
				car.noCommand();
				frontLight = light.getLightValue();
			}

			// Follow light as long as the light level is above the threshold
			while (frontLight > lightThreshold) {
				// Get the light to the left
				while (motor.getTachoCount() < angle) {
					motor.controlMotor(powerHorizontal, forward);
				}
				motor.controlMotor(powerHorizontal, stop);
				leftLight = light.getLightValue();

				// Get the light to the right
				while (motor.getTachoCount() > -angle) {
					motor.controlMotor(powerHorizontal, backward);
				}
				motor.controlMotor(0, stop);
				rightLight = light.getLightValue();

				// Turn back to start position
				while (motor.getTachoCount() < 0) {
					motor.controlMotor(powerHorizontal, forward);
				}
				motor.controlMotor(0, stop);

				// Follow light for a while
				delta = leftLight - rightLight;
				car.forward(power - delta, power + delta);
				Delay.msDelay(ms);

				frontLight = light.getLightValue();
			}

			car.stop();
			Delay.msDelay(ms);

		}
	}
}
