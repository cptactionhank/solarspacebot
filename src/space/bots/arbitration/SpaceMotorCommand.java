package space.bots.arbitration;

public enum SpaceMotorCommand {
	FORWARD, BACKWARD, STOP, IGNORE, TURN
}