package space.bots.arbitration;

public class SpaceBotCommand {
	
	public volatile SpaceMotorCommand command = SpaceMotorCommand.IGNORE;
	public volatile SpaceMotorCommand pickupCommand = SpaceMotorCommand.IGNORE;
	public volatile int leftPower, rightPower, pickupPower;
	public volatile int turn;

}
