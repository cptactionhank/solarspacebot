package space.bots.arbitration;

import space.bots.SpaceBotDriver;
import space.bots.behaviors.Behavior;

/*
 *  Arbiter pass the highest priority car command
 *  from the behaviors to the car driver.   
 */
public class Arbiter implements Runnable {

	private final Behavior[] behaviors;
	private final SpaceBotDriver driver;
	private final Thread thread;
	private int winner = -1;

	public Arbiter(SpaceBotDriver cd, Behavior... behaviors) {
		thread = new Thread(this);
		thread.setDaemon(true);
		thread.setName("Arbiter Thread");
		this.behaviors = behaviors;
		this.driver = cd;
	}
	
	public void start() {
		// start all behaviors
		for(Behavior b : behaviors)
			b.start();
		// start arbiter loop
		thread.start();
	}
	
	public void run() {
		Behavior b;
		while (true) {
			// iterate each behavior
			winner = -1;
			for (int i = 0; i < behaviors.length; i++) {
				b = behaviors[i];
				// request what to do
				SpaceBotCommand carCommand = b.requestCommand();
				// if there is a command then perform it
				if (carCommand != null && winner == -1) {
					winner = i;
					// perform the command
					driver.perform(carCommand);
					// notify the behavior won
					b.won();
				} else {
					// notify the behavior it lost
					b.lost();
				}
			}
		}
	}

	public int winner() {
		return winner;
	}
}
