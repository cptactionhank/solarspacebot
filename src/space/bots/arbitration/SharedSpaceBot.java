package space.bots.arbitration;

import space.bots.SpaceBot;

/**
 * A locomotion module with methods to drive a differential car with two
 * independent motors. The methods turns the parameters for the two motors into
 * commands for a car driver that will fetch the commands and turn them into
 * motor commands for the physical motors.
 * 
 * @author Ole Caprani
 * @version 26.3.14
 */
public class SharedSpaceBot implements SpaceBot {

	private boolean commandReady = false;
	private SpaceBotCommand carCommand = new SpaceBotCommand();
	int power_left = 0;
	int power_right = 0;
	private float scale_left = 1.0f;
	private float scale_right = 1.0f;

	public void stop() {
		carCommand.command = SpaceMotorCommand.STOP;
		carCommand.leftPower = 0;
		carCommand.rightPower = 0;
		commandReady = true;
	}

	public void forward(int leftPower, int rightPower) {
		carCommand.command = SpaceMotorCommand.FORWARD;
		power_left = leftPower;
		power_right = rightPower;
		scale(scale_left, scale_right);
		commandReady = true;
	}

	public void backward(int leftPower, int rightPower) {
		carCommand.command = SpaceMotorCommand.BACKWARD;
		power_left = leftPower;
		power_right = rightPower;
		scale(scale_left, scale_right);
		commandReady = true;
	}

	@Override
	public void pickUp(int power) {
		carCommand.pickupCommand = SpaceMotorCommand.BACKWARD;
		carCommand.pickupPower = power;
		commandReady = true;
	}

	@Override
	public void spitOut(int power) {
		carCommand.pickupCommand = SpaceMotorCommand.FORWARD;
		carCommand.pickupPower = power;
		commandReady = true;
	}

	@Override
	public void stopPickup() {
		carCommand.pickupCommand = SpaceMotorCommand.STOP;
		carCommand.pickupPower = 0;
		commandReady = true;
	}

	public void noCommand() {
		commandReady = false;
	}
	
	public boolean isReady(){
		return commandReady;
	}

	public SpaceBotCommand getCommand() {
		return commandReady ? carCommand : null;
	}

	@Override
	public void scale(float left, float right) {
		scale_left = left;
		scale_right = right;
		carCommand.leftPower = Math.round(this.power_left * left);
		carCommand.rightPower = Math.round(this.power_right * right);
		commandReady = true;
	}

	public void turn(int i) {
		carCommand.command = SpaceMotorCommand.TURN;
		carCommand.turn = i;
		commandReady = true;
	}

}
