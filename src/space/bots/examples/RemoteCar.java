package space.bots.examples;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.util.Delay;
import space.bots.SpaceBotDriver;
import space.bots.arbitration.Arbiter;
import space.bots.behaviors.Behavior;
import space.bots.solar.behaviors.RemoteDirectionalBehavior;
import dk.pmm.ControlConstants;

public class RemoteCar extends space.bots.communication.RemoteBotBase {
	// public class SolarPanelBot extends space.bots.BotBase {

	public static void main(String[] args) throws Exception {
		RemoteCar bot = new RemoteCar();
		bot.setup();
		bot.run();
	}

	private final Arbiter arbiter;
	private final Behavior[] behaviors;

	public RemoteCar() {
		// save reference to this robot
		final RemoteCar bot = this;
		// create space robot driver
		SpaceBotDriver cd = new SpaceBotDriver();
		// behaviors
		RemoteDirectionalBehavior remoteBehavior = new RemoteDirectionalBehavior(
				ControlConstants.OFFSET_REMOTE, this) {
			@Override
			public void exception(Thread t, Throwable e) {
				bot.exception(t, e);
			}
		};
		// setup listener
		addObserver(remoteBehavior);

		// create array of behaviors to start
		behaviors = new Behavior[] { remoteBehavior };
		// Create arbiter
		arbiter = new Arbiter(cd, behaviors);
	}

	@Override
	public void run() {
		// start the behaviors
		for (Behavior behavior : behaviors) {
			behavior.start();
		}
		// start the arbiter
		arbiter.start();

		// == main control loop ==

		LCD.drawString("Ready", 0, 0);

		// initialize sensor readings
		UltrasonicSensor sensor = new UltrasonicSensor(SensorPort.S1);

		MotorPort.A.resetTachoCount();
		MotorPort.B.resetTachoCount();
		MotorPort.C.resetTachoCount();

		// speed up logging
		super.logDelay = 80;

		while (!Button.ESCAPE.isDown()) {
			log(0, sensor.getDistance());
			log(1, MotorPort.A.getTachoCount(), MotorPort.B.getTachoCount(),
					MotorPort.C.getTachoCount());
			Delay.msDelay(super.logDelay);
		}

	}
}
