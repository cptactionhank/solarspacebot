package space.bots.examples;

import lejos.nxt.Button;
import lejos.util.Delay;

//public class TestProgram extends space.bots.BotBase {
public class RemoteExample extends space.bots.communication.RemoteBotBase {

	public static void main(String[] args) {
		RemoteExample example = new RemoteExample();
		example.setup();
		example.run();
	}

	@Override
	public void run() {
		// sensor loop
		while (!Button.ESCAPE.isDown()) {
			// log current time
			log(0, System.currentTimeMillis());
			// ease up on the sensor readings
			Delay.msDelay(super.logDelay);
		}
	}

}