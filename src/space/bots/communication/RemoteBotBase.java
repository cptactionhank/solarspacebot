package space.bots.communication;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import lejos.nxt.Sound;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import space.bots.BotBase;
import dk.pmm.messaging.Messenger;
import dk.pmm.messaging.RemoteMessenger;
import dk.pmm.messaging.events.MessagingEventObservable;
import dk.pmm.messaging.events.MessagingEventObserver;

public abstract class RemoteBotBase extends BotBase implements Messenger {

	// remote messenger lock
	protected final Object MSGRLOCK = new Object();
	// remote messenger client
	protected Messenger messenger;
	protected final MessagingEventObservable observable = new MessagingEventObservable();

	public RemoteBotBase() {
		// save a reference for this instance
		final RemoteBotBase _this = this;
		// create thread for awaiting incoming Remote Messenger connection
		Thread listener = new Thread(new Runnable() {

			@Override
			public void run() {
				// save a reference for the thread, so we can interrupt later
				final Thread _thread = Thread.currentThread();
				// run a connection listening loop while the thread is not
				// interrupted.
				while (!Thread.interrupted()) {
					// await a bluetooth connection
					final BTConnection connection = Bluetooth
							.waitForConnection();
					// open input/output
					final InputStream input = connection.openInputStream();
					final OutputStream output = connection.openOutputStream();
					// aquire the messenger lock
					synchronized (MSGRLOCK) {
						// setup the new remote messenger
						_this.messenger = new RemoteMessenger(input, output) {

							// shutdown the lego program
							protected void onKillCommand() {
								try {
									synchronized (MSGRLOCK) {
										_this.messenger
												.message("So were are going to shut down now");
									}
								} catch (IOException e1) {
									exception(e1);
								}
								// interrupt the listening thread
								_thread.interrupt();
								// exit with success
								System.exit(0);
							};

						};
					}
					messenger.addObserver(new MessagingEventObserver() {
						public void update(MessagingEventObservable o,
								Object arg) {
							_this.observable.notifyObservers(arg);
						};
					});

					Sound.beepSequenceUp();
					// listen for incoming commands
					try {
						// start the listening loop
						messenger.listen();
					} catch (Exception e) {
						// log the exception which occurred
						exception(e);
					}
					// disable the messenger
					synchronized (MSGRLOCK) {
						messenger.removeObservers();
						messenger = null;
					}
					// close the streams or log exception
					try {
						input.close();
						output.close();
					} catch (IOException e) {
						exception(e);
					}
					// close the connection
					connection.close();
					Sound.beepSequence();
				}
			}
		});
		// setup listener thread properties
		listener.setName("Bluetooth Worker");
		listener.setDaemon(true);
		listener.setPriority(8);
		listener.start();
	}

	@Override
	public void log(int channel, Object... args) {
		try {
			logThrottled(channel, true, args);
		} catch (IOException e) {
			exception(e);
		}
	}

	@Override
	public void logThrottled(int channel, boolean throttle, Object... args)
			throws IOException {
		logThrottledInterval(channel, throttle, logDelay, args);
	}

	@Override
	public synchronized void logThrottledInterval(int channel,
			boolean throttle, int throttleInterval, Object... args)
			throws IOException {
		synchronized (MSGRLOCK) {
			// if there is a messenger present log the inputs; otherwise skip
			if (messenger != null) {
				try {
					messenger.logThrottledInterval(channel, true,
							throttleInterval, args);
				} catch (Throwable e) {
					exception(e);
				}
			}
		}
	}

	@Override
	public synchronized void exception(Thread t, Throwable e) {
		// sound scary
		Sound.buzz();
		// lock
		synchronized (MSGRLOCK) {
			// if the messenger is available try and send the exception
			if (messenger != null) {
				try {
					// convert stack trace to string
					ByteArrayOutputStream foo = new ByteArrayOutputStream(0);
					e.printStackTrace(new PrintStream(foo));
					// send the exception to the logging server
					messenger.message("exception in thread '" + t.getName()
							+ "' \nwith error: " + e.toString() + "\n"
							+ foo.toString("UTF-8"));
					foo.close();
				} catch (Throwable e1) {
					// log hard exception based on error in remote communication
					super.exception(t, e1);
				}
			} else {
				super.exception(t, e);
			}
		}
	}

	@Override
	public void kill() throws IOException {
		synchronized (MSGRLOCK) {
			if (messenger != null) {
				messenger.kill();
			}
		}
	}

	@Override
	public void listen() throws Exception {
		throw new Exception("This does not make sense");
	}

	@Override
	public void message(String string) throws IOException {
		synchronized (MSGRLOCK) {
			if (messenger != null) {
				messenger.message(string);
			}
		}
	}

	@Override
	public void vendorCommand(int id, Object value) throws IOException {
		synchronized (MSGRLOCK) {
			if (messenger != null) {
				messenger.vendorCommand(id, value);
			}
		}
	}

	public void addObserver(MessagingEventObserver observer) {
		this.observable.addObserver(observer);
	}

	public void removeObserver(MessagingEventObserver observer) {
		this.observable.deleteObserver(observer);
	}

	public void removeObservers() {
		this.observable.deleteObservers();
	}
}
