package space.bots;

public interface SpaceBot {
	
	public void scale(float scale_left, float scale_right);
	
	public void stop();

	public void forward(int power_left, int power_right);

	public void backward(int power_left, int power_right);

	public void pickUp(int pickupPower);

	public void spitOut(int spitOutPower);

	public void stopPickup();

}
