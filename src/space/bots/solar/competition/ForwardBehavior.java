package space.bots.solar.competition;

import space.bots.behaviors.BaseBehavior;
import space.bots.solar.data.CompetitionState;

public class ForwardBehavior extends BaseBehavior {

	public ForwardBehavior() {
		bot.forward(CompetitionState.SPEED, CompetitionState.SPEED);
	}

	
}
