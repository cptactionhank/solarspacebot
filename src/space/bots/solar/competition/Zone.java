package space.bots.solar.competition;

public enum Zone {
	HOME, STORAGE, BRIDGE, WORLD, PANELS
}
