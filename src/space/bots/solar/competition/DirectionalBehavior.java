package space.bots.solar.competition;

import static space.bots.solar.competition.Location.HOME;
import static space.bots.solar.competition.Location.INTERSECTION0;
import static space.bots.solar.competition.Location.INTERSECTION1;
import static space.bots.solar.competition.Location.INTERSECTION4;
import static space.bots.solar.competition.Location.PANEL1;
import static space.bots.solar.competition.Location.PANEL2;
import static space.bots.solar.competition.Location.PANEL3;
import static space.bots.solar.competition.Location.PANEL4;
import static space.bots.solar.competition.Location.PANEL5;

import java.util.Arrays;

import lejos.nxt.Sound;
import lejos.util.Delay;
import space.bots.behaviors.ThreadedBehavior;
import space.bots.solar.data.Colors;
import space.bots.solar.data.CompetitionState;
import space.bots.solar.data.SensorReadings;
import dk.pmm.messaging.Messenger;

public class DirectionalBehavior extends ThreadedBehavior {

	boolean out = false;
	int count = 0;

	Location current_location = Location.HOME;
	Location current_direction = INTERSECTION0;
	Location goal_location = PANEL4;

	Location[][] matrix = new Location[max() + 1][max() + 1];

	public DirectionalBehavior(final Messenger messenger) {
		super();
		// pre-fill the location matrix with UNKNOWN location
		for (Location[] xs : matrix)
			Arrays.fill(xs, Location.UNKNOWN);
		// set location data
		Arrays.fill(matrix[o(HOME)], INTERSECTION0);
		Arrays.fill(matrix[o(INTERSECTION0)], INTERSECTION1);
		Arrays.fill(matrix[o(INTERSECTION1)], INTERSECTION4);
		Arrays.fill(matrix[o(INTERSECTION4)], PANEL3);
		Arrays.fill(matrix[o(PANEL3)], PANEL4);
		Arrays.fill(matrix[o(PANEL4)], PANEL5);
		Arrays.fill(matrix[o(PANEL5)], PANEL2);
		Arrays.fill(matrix[o(PANEL2)], PANEL1);
		// next is self for from and to
		for (Location l : Location.values())
			matrix[o(l)][o(l)] = l;
	}

	private final static int o(Location l) {
		return l.ordinal();
	}

	@Override
	public void run() {
		out = false;
		// start by leaving home
		bot.forward(CompetitionState.SPEED, CompetitionState.SPEED);
		while (Colors.isBlack(SensorReadings.colorSideNormalized) == false)
			Thread.yield();
		Sound.twoBeeps();
		count = 1;
		out = true;
		bot.noCommand();
		Delay.msDelay(100);
		// sensor aggregation loop
		while (true) {
			// wait while command is being performed
			while (bot.isReady())
				Thread.yield();
			// wait until we see a black color
			while (Colors.isBlack(SensorReadings.colorSideNormalized) == false)
				Thread.yield();
			// we saw a black color, now do something
			Location now = calc_location(current_location, current_direction);
			Location next_dir = calc_direction(current_location, goal_location);
			// steer the robot
			steer(current_location, now, next_dir);
			// update state
			update(now, next_dir);
			Delay.msDelay(1000);
		}
	}

	private void steer(Location from, Location now, Location to) {
		count += 1;
		Sound.beep();
		if (count == 2)
			bot.turn(-90);
	}

	private Location calc_location(Location from, Location dir) {
		if (from == HOME)
			return INTERSECTION0;
		else if (from == INTERSECTION0)
			return INTERSECTION1;
		else if (from == INTERSECTION1)
			return INTERSECTION4;
		else if (from == INTERSECTION4)
			return PANEL3;
		else if (from == PANEL3)
			return PANEL4;
		else if (from == PANEL4)
			return PANEL2;
		else if (from == PANEL2)
			return PANEL1;
		else
			return Location.UNKNOWN;
	}

	private int max() {
		int max = Integer.MIN_VALUE;
		for (Location l : Location.values())
			max = Math.max(max, l.ordinal());
		return max;
	}

	private Location calc_direction(Location from, Location to) {
		return matrix[from.ordinal()][to.ordinal()];
	}

	private void update(Location l, Location d) {
		current_location = l;
		current_direction = d;
	}

	@Override
	public void won() {
		super.won();
		if (out)
			// release control if not in home zone
			bot.noCommand();
	}
}
