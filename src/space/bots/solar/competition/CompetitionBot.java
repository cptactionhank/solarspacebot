package space.bots.solar.competition;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import space.bots.SpaceBotDriver;
import space.bots.arbitration.Arbiter;
import space.bots.behaviors.BaseBehavior;
import space.bots.solar.behaviors.PIDBehavior;
import space.bots.solar.behaviors.RemoteDirectionalBehavior;
import space.bots.solar.data.Colors;
import space.bots.solar.data.CompetitionState;
import space.bots.solar.data.SensorReadings;
import dk.pmm.ControlConstants;

public class CompetitionBot extends space.bots.communication.RemoteBotBase {

	public static void main(String[] args) throws Exception {
		// give the sensor thread highest priority
		Thread.currentThread().setPriority(8);
		// create and run the competition bot
		CompetitionBot bot = new CompetitionBot();
		bot.setup();
		bot.run();
	}

	private final Arbiter arbiter;
	private ColorSensor colorSensorSide;
	private LightSensor lightSensorFront;

	public CompetitionBot() {

		logDelay = 100;

		// ### SENSOR SETUP
		colorSensorSide = new ColorSensor(SensorPort.S1);
		lightSensorFront = new LightSensor(SensorPort.S2, true);

		// ### ROBOT DRIVER
		SpaceBotDriver cd = new SpaceBotDriver();

		// ### BEHAVIORS

		// Create arbiter
		arbiter = new Arbiter(cd,
				new RemoteDirectionalBehavior(ControlConstants.OFFSET_REMOTE, this, CompetitionState.SPEED), 
				new PIDBehavior(ControlConstants.OFFSET_PID, this, Colors.FRONT_BLACK),
				// new DirectionalBehavior(this),
				// new ForwardBehavior(),
				// null behavior
				new BaseBehavior());
	}

	@Override
	public void run() {

		// ### initialize LCD screen
		LCD.clear();
		LCD.drawString("SolarSpaceBot", 0, 0);

		// start the arbiter
		arbiter.start();

		// ### main control loop
		do {

			// read from the sensors
			SensorReadings.colorSideNormalized = colorSensorSide
					.getNormalizedLightValue();
			SensorReadings.lightFront = lightSensorFront
					.getNormalizedLightValue();

			// log the recorded data
			log(0, arbiter.winner(), SensorReadings.lightFront,
					SensorReadings.colorIdUnder, SensorReadings.lightUnder,
					SensorReadings.colorIdSide,
					SensorReadings.colorSideNormalized);
			if (Button.LEFT.isDown())
				throw new RuntimeException(
						"Oh noes en runtime exception under demonstrationen");
		} while (!Button.ESCAPE.isDown());

		LCD.clear();
		LCD.drawString("SpaceBot Done...", 0, 0);
		System.exit(0);
	}

}
