package space.bots.solar;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.util.Delay;
import space.bots.SpaceBotDriver;
import space.bots.arbitration.Arbiter;
import space.bots.arbitration.SharedSpaceBot;
import space.bots.behaviors.Behavior;
import space.bots.solar.behaviors.PIDFollowerBehavior;
import space.bots.solar.behaviors.PickupBrickBehavior;
import space.bots.solar.behaviors.RemoteDirectionalBehavior;
import space.bots.solar.data.SensorReadings;
import dk.pmm.ControlConstants;
import dk.pmm.messaging.RemoteMessenger;

/*
 * Behavior control network of Figure 9.9 in chapter 9 in 2
 * Jones, Flynn, and Seiger: 
 * "Mobile Robots, Inspiration to Implementation", 
 * Second Edition, 1999.
 */

public class SolarPanelBot extends space.bots.communication.RemoteBotBase {
	// public class SolarPanelBot extends space.bots.BotBase {

	public static void main(String[] args) throws Exception {

		SolarPanelBot bot = new SolarPanelBot();
		bot.setup();
		bot.run();
	}

	private final Arbiter arbiter;
	private final Behavior[] behaviors;
	private ColorSensor colorSensorUnder;
	private ColorSensor colorSensorSide;
	
	
	private LightSensor lightSensorFront;
	private LightSensor lightSensorBack;
	private SharedSpaceBot[] cars;

	String white = "White:";
	String black = "Black:";
	String green = "Green:";
	
	public SolarPanelBot() {
		
		// save reference to this robot
		final SolarPanelBot bot = this;

		
		colorSensorUnder = new ColorSensor(SensorPort.S4);
		lightSensorFront = new LightSensor(SensorPort.S2);
		lightSensorFront.setFloodlight(true);
		lightSensorBack = new LightSensor(SensorPort.S3);
		lightSensorBack.setFloodlight(true);
		colorSensorSide = new ColorSensor(SensorPort.S1);
		
		Delay.msDelay(500);
		
		SensorReadings.colorIdUnder = colorSensorUnder.getColorID();
		SensorReadings.lightFront = lightSensorFront.getNormalizedLightValue();
		SensorReadings.lightBack = lightSensorBack.getNormalizedLightValue();
		SensorReadings.colorSideNormalized = colorSensorUnder.getNormalizedLightValue();
		
		init();
		
		
		
		cars = new SharedSpaceBot[] { new SharedSpaceBot(), new SharedSpaceBot(),
				new SharedSpaceBot(), new SharedSpaceBot() };
		// create space robot driver
		SpaceBotDriver cd = new SpaceBotDriver();
		// behaviors
		//CalibrateLight behaviorCalibrateLight = new CalibrateLight(cars[1]);

		PIDFollowerBehavior behaviorFollowLine = new PIDFollowerBehavior(cars[1]);
		//PickupBrickBehavior pickupBrickBehaviour = new PickupBrickBehavior(cars[1]);

		RemoteDirectionalBehavior remoteBehavior = new RemoteDirectionalBehavior(
				ControlConstants.OFFSET_REMOTE, this) {
			@Override
			public void exception(Thread t, Throwable e) {
				bot.exception(t, e);
			}
		};

		// setup listener
		addObserver(remoteBehavior);

		// create array of behaviors to start
		behaviors = new Behavior[] { remoteBehavior, behaviorFollowLine};
		// Create arbiter
		arbiter = new Arbiter(cd, behaviors);
	}

	@Override
	public void run() {
		// start the behaviors
		for (Behavior behavior : behaviors) {
			behavior.start();
		}
		// start the arbiter
		arbiter.start();

		// == main control loop ==

		LCD.drawString("SolarSpaceBot", 0, 0);

		SensorReadings.colorIdUnder = colorSensorUnder.getColorID();
		SensorReadings.colorSideNormalized = colorSensorSide.getNormalizedLightValue();
		SensorReadings.lightFront = lightSensorFront.getNormalizedLightValue();
		SensorReadings.lightBack = lightSensorBack.getNormalizedLightValue();

		MotorPort.A.resetTachoCount();
		MotorPort.B.resetTachoCount();
		MotorPort.C.resetTachoCount();

		while (!Button.ESCAPE.isDown()) {
			
			SensorReadings.colorIdUnder = colorSensorUnder.getColorID();
			SensorReadings.colorSideNormalized = colorSensorSide.getNormalizedLightValue();
			SensorReadings.lightFront = lightSensorFront
					.getNormalizedLightValue();
			SensorReadings.lightBack = lightSensorBack
					.getNormalizedLightValue();
			SensorReadings.redRawUnder = colorSensorUnder.getRawColor().getRed();
			SensorReadings.greenRawUnder = colorSensorUnder.getRawColor().getGreen();
			SensorReadings.blueRawUnder = colorSensorUnder.getRawColor().getBlue();

			SensorReadings.tachoCountA = MotorPort.A.getTachoCount();
			SensorReadings.tachoCountB = MotorPort.B.getTachoCount();

			updatePID();
			
			LCD.drawString("Winner", 0, 1);
			LCD.drawInt(arbiter.winner(), 1, 8, 1);
			
			log(0, arbiter.winner(), colorSensorUnder.getColorID(), colorSensorUnder.getRawColor().getRed(), colorSensorUnder.getRawColor().getGreen(), colorSensorUnder.getRawColor().getBlue());
			Delay.msDelay(10);
		}

		LCD.clear();
		LCD.drawString("SpaceBot Done...", 0, 0);
	}
	
	private void updatePID() {
		float scale = 1.0f;
		int Kc = 50;
		float dT = 0.0015f; // 0,0015 sek per loop
		int Pc = 1; // 1 sekund

		float Kp = 25; // 0.6f*Kc; // REMEMBER we are using Kp*100 so this is
							// really 10 //
		float Ki = 0; // (2*Kp*dT)/Pc; // REMEMBER we are using Ki*100 so
								// this is really 1 //

		float Kd = 0; // (Kp*Pc)/(8*dT); // REMEMBER we are using Kd*100 so
								// this is really 100//
		int offset; // Initialize the variables
		int Tp = 70;
		float integral = 0; // the place where we will store our integral
		float lastError = 0; // the place where we will store the last error
									// value
		float derivative = 0; // the place where we will store the derivative

		
		int lightValue;
		lightValue = SensorReadings.lightFront; // what is the current
		// light reading?	
		//TODO
		offset = SensorReadings.black + 65;
		
		float error = lightValue - offset; // calculate the error by
		// subtracting the offset
		integral = integral + error; // calculate the integral
		derivative = error - lastError; // calculate the derivative
		float turn = Kp * error + Ki * integral + Kd * derivative; // the
		// "P term"
		// the
		// "I term"
		// and
		// the
		// "D term"
		turn = turn / 100f; // REMEMBER to undo the affect of the factor of
		// 100 in Kp, Ki and Kd//
		SensorReadings.powerA = Tp - (int) turn; // the power level for the A motor
		SensorReadings.powerC = Tp + (int) turn; // the power level for the C motor
	}

	private void init(){
		int power = 70;
		Delay.msDelay(500);
		SensorReadings.colorBlackNormalized = colorSensorSide.getNormalizedLightValue();
		int lightValue = lightSensorFront.getNormalizedLightValue();
		int currColorvalue = colorSensorSide.getNormalizedLightValue();
		SensorReadings.black = lightSensorFront.getNormalizedLightValue();
		SensorReadings.green = SensorReadings.black;
		MotorPort.A.resetTachoCount();
		int tachoCounter = MotorPort.A.getTachoCount();
		
		while (tachoCounter < 580) {
			tachoCounter = MotorPort.A.getTachoCount();
			lightValue = lightSensorFront.getNormalizedLightValue();
			currColorvalue = colorSensorSide.getNormalizedLightValue();
			MotorPort.A.controlMotor(power, 1);
			MotorPort.B.controlMotor(power+3, 1);
			if(lightValue != 0){
				if (lightValue < SensorReadings.black) {
					SensorReadings.black = lightValue;
				}
			}
			if (lightValue > SensorReadings.white) {
				SensorReadings.white = lightValue;
			}
			if(SensorReadings.colorBlackNormalized > currColorvalue){
				SensorReadings.colorBlackNormalized = currColorvalue;
			}			
		}
		
		while(lightValue>SensorReadings.black+75){
			lightValue = lightSensorFront.getNormalizedLightValue();
		}
		while(lightValue<SensorReadings.black+85){
			lightValue = lightSensorFront.getNormalizedLightValue();
		}
		MotorPort.A.controlMotor(0, 3);
		MotorPort.B.controlMotor(power+3, 1);
		
		while(lightValue>SensorReadings.black+80){
			lightValue = lightSensorFront.getNormalizedLightValue();
		}
		while(lightValue<SensorReadings.black+85){
			lightValue = lightSensorFront.getNormalizedLightValue();
		}
		
		SensorReadings.colorSideNormalized = colorSensorSide.getNormalizedLightValue();
		LCD.drawString(white, 0, 3);
		LCD.drawString(black, 0, 4);
		LCD.drawString(green, 0, 5);
		
		LCD.drawInt(SensorReadings.white, 3, 7, 3);
		LCD.drawInt(SensorReadings.black, 3, 7, 4);
		LCD.drawInt(SensorReadings.green, 3, 7, 5);
//		
//		while(true)
//			MotorPort.B.controlMotor(0, 3);
		
//		MotorPort.B.resetTachoCount();
//		tachoCounter = MotorPort.B.getTachoCount();
//		while (tachoCounter<610){
//			tachoCounter = MotorPort.B.getTachoCount();
//			MotorPort.A.controlMotor(50, 1);
//			MotorPort.B.controlMotor(power+3, 1);
//		}
//		MotorPort.A.controlMotor(0, 3);
//		MotorPort.B.controlMotor(0, 3);
	}
}
