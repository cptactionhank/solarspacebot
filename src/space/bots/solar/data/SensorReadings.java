package space.bots.solar.data;

public class SensorReadings {

	public static int colorIdUnder;
	public static int redRawUnder;
	public static int greenRawUnder;
	public static int blueRawUnder;
	
	public static int colorSideNormalized;
	public static int lightFront;
	public static int lightBack;
	public static int tachoCountA;
	public static int tachoCountB;
	
	public static int black;
	public static int colorBlackNormalized;
	public static int green;
	public static int white;
	
	public static int powerA;
	public static int powerC;
	public static int lightUnder;
	public static int colorIdSide;

}
