package space.bots.solar.data;

public class Colors {
	
	public static final int FRONT_BLACK = 360;
	
	public static final int BLACK_LOW = 60;
	public static final int BLACK_HIGH = 200;
	
	public static final int GREEN_LOW = 255;
	public static final int GREEN_HIGH = 280;
	
	public static boolean isBlack(int value) {
		return value <= BLACK_HIGH && value >= BLACK_LOW;
	}

	public static boolean isGreen(int value) {
		return value <= GREEN_HIGH && value >= GREEN_LOW;
	}

}
