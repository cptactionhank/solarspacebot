package space.bots.solar.data;

import space.bots.solar.competition.Location;
import space.bots.solar.competition.Task;
import space.bots.solar.competition.Zone;

public class CompetitionState {

	public static int SPEED = 200;

	public static Zone current_zone = Zone.HOME;
	public static Task current_task = Task.REPAIR;
	public static Location current_location = Location.HOME;

	public static Zone goal_zone = Zone.PANELS;
	public static Task goal_task = Task.REPAIR;
	public static Location goal_location = Location.PANEL3;
}
