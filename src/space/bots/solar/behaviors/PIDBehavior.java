package space.bots.solar.behaviors;

import space.bots.behaviors.ThreadedBehavior;
import space.bots.solar.data.CompetitionState;
import space.bots.solar.data.SensorReadings;
import dk.pmm.ControlConstants;
import dk.pmm.controls.pid.PidControl;
import dk.pmm.controls.pid.RemotePidControl;
import dk.pmm.messaging.Messenger;

public class PIDBehavior extends ThreadedBehavior {

	private final PidControl control;

	// scale
	static float scale = 1f / 100f;
	// critical gain factor
	// static int Kc = 50;
	// Loop iteration time: 60(ms) / 1000(ms/s)
	// static float dT = 60f / 1000f;
	// Oscillation period length: xx(ms) / 1000(ms/s)
	// static int Pc = 10;

	// PID guys
	static float Kp = 130;
	static float Ki = 0;
	static float Kd = 0;
	// initialize the variables
	static int offset;
	// initial power
	static int Tp = CompetitionState.SPEED;

	// ### running state
	// the place where we will store our integral
	static float integral = 0;
	// the place where we will store the last error value
	static float lastError = 0;
	// the place where we will store the derivative
	static float derivative = 0;

	public PIDBehavior(int id_offset, final Messenger messenger, int initial_offset) {
		super();
		this.setName("PID Line follow behavior");
		offset = initial_offset;
		control = new RemotePidControl(id_offset, messenger) {

			@Override
			public void setOffset(int value) {
				offset = value;
			}

			@Override
			public void setPower(int value) {
				Tp = value;
			}

			@Override
			public void setKp(float value) {
				Kp = value;
			}

			@Override
			public void setKi(float value) {
				Ki = value;
			}

			@Override
			public void setKd(float value) {
				Kd = value;
			}

			@Override
			public void setScale(float value) {
				scale = value;
			}

			@Override
			public void report() {
				super.setKd(Kd);
				super.setKi(Ki);
				super.setKp(Kp);
				super.setOffset(offset);
				super.setPower(Tp);
				super.setScale(scale);
			}

			@Override
			public void reset() {
				// reset PID state
				lastError = 0;
				integral = 0;
				derivative = 0;
			}

		};

		messenger.addObserver(control);

	}
	
	public void run() {
		while (true) {
			// what is the current light reading?
			int lightValue = SensorReadings.lightFront;
			// calculate the error by subtracting the offset
			float error = lightValue - offset;
			// calculate the integral
			integral = integral + error;
			// calculate the derivative
			derivative = error - lastError;
			// the "P term" the "I term" and the "D term"
			float turn = Kp * error + Ki * integral + Kd * derivative;
			// REMEMBER to undo the affect of the factor of 100 in Kp, Ki and Kd
			turn = turn * scale;
			// the power level for the A motor
			int powerLeft = Tp - (int) turn;
			// the power level for the C motor
			int powerRight = Tp + (int) turn;
			// set the powers
			bot.forward(powerLeft, powerRight);
			// save the current error so it can be the lastError next time
			// around
			lastError = error;
			// allow a little breathing room
			Thread.yield();
			// log the PID running state back
			control.log(ControlConstants.CHANNEL_PID, error, integral,
					derivative, turn, powerLeft, powerRight);
		}
	}

	@Override
	public void lost() {
		control.reset();
	}
}