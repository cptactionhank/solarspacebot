package space.bots.solar.behaviors;

import lejos.nxt.Sound;
import lejos.util.Delay;
import space.bots.arbitration.SharedSpaceBot;
import space.bots.arbitration.SpaceBotCommand;
import space.bots.behaviors.Behavior;
import space.bots.solar.data.CompetitionState;
import dk.pmm.controls.DirectionalControl;
import dk.pmm.messaging.Messenger;

public class RemoteDirectionalBehavior extends DirectionalControl implements
		Behavior {

	private SharedSpaceBot car = new SharedSpaceBot();
	private int baseSpeed = CompetitionState.SPEED;

	public RemoteDirectionalBehavior(int offset, Messenger messenger) {
		super(offset);
		messenger.addObserver(this);
	}

	public RemoteDirectionalBehavior(int offset, Messenger messenger, int speed) {
		this(offset, messenger);
		baseSpeed = speed;
	}

	@Override
	public void north(boolean stop) {
		if (stop) {
			car.stop();
			Delay.msDelay(50);
			car.noCommand();
		} else {
			car.forward(baseSpeed, baseSpeed);
		}
	}

	@Override
	public void south(boolean stop) {
		if (stop) {
			car.stop();
			Delay.msDelay(50);
			car.noCommand();
		} else {
			car.backward(baseSpeed, baseSpeed);
		}
	}

	@Override
	public void east(boolean stop) {
		if (stop) {
			car.scale(1, 1);
		} else {
			car.scale(1.0f, 0.25f);
		}
	}

	@Override
	public void west(boolean stop) {
		if (stop) {
			car.scale(1, 1);
		} else {
			car.scale(0.25f, 1.0f);
		}
	}

	@Override
	public void boost(boolean stop) {
		if (stop) {
			car.scale(1, 1);
			Sound.buzz();
		} else {
			car.scale(2, 2);
			Sound.beep();
		}
	}

	@Override
	public SpaceBotCommand requestCommand() {
		return car.getCommand();
	}

	@Override
	public void won() {
		// noop
	}

	@Override
	public void lost() {
		// lost
	}

	@Override
	public void start() {
		// noop
	}

}
