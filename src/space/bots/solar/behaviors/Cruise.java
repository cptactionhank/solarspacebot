package space.bots.solar.behaviors;

import space.bots.arbitration.SharedSpaceBot;

public class Cruise extends Thread {
	private SharedSpaceBot car;

	public Cruise(SharedSpaceBot car) {
		this.setDaemon(true);
		this.car = car;
	}

	public void run() {
		while (true) {
			car.noCommand();
		}
	}
}
