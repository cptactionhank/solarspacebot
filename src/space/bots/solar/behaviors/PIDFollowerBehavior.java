package space.bots.solar.behaviors;

import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.nxt.Sound;
import lejos.robotics.Color;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;
import space.bots.arbitration.SharedSpaceBot;
import space.bots.behaviors.ThreadedBehavior;
import space.bots.solar.data.SensorReadings;
import dk.pmm.ControlConstants;
import dk.pmm.controls.pid.PidControl;
import dk.pmm.controls.pid.RemotePidControl;
import dk.pmm.messaging.RemoteMessenger;

public class PIDFollowerBehavior extends ThreadedBehavior {

	//private final PidControl control;
	private final SharedSpaceBot car;

	static float scale = 1.0f;
	static int Kc = 50;
	static float dT = 0.0015f; // 0,0015 sek per loop
	static int Pc = 1; // 1 sekund

	static float Kp = 25; // 0.6f*Kc; // REMEMBER we are using Kp*100 so this is
							// really 10 //
	static float Ki = 0; // (2*Kp*dT)/Pc; // REMEMBER we are using Ki*100 so
							// this is really 1 //

	static float Kd = 0; // (Kp*Pc)/(8*dT); // REMEMBER we are using Kd*100 so
							// this is really 100//
	static int offset; // Initialize the variables
	static int Tp = 70;
	static float integral = 0; // the place where we will store our integral
	static float lastError = 0; // the place where we will store the last error
								// value
	static float derivative = 0; // the place where we will store the derivative

	private boolean crossSeen = false;
	private boolean useFrontSensor = true;
	
	private DifferentialPilot pilot = new DifferentialPilot(7.0, 22.0, Motor.B, Motor.A, true);

	public PIDFollowerBehavior(SharedSpaceBot car) { //
			
		//final RemoteMessenger messenger
		
		this.setDaemon(true);
		this.car = car;
		this.setName("FollowLine");

//		control = new RemotePidControl(ControlConstants.OFFSET_PID, messenger) {
//
//			@Override
//			public void setOffset(int value) {
//				offset = value;
//			}
//
//			@Override
//			public void setPower(int value) {
//				Tp = value;
//			}
//
//			@Override
//			public void setKp(float value) {
//				Kp = value;
//			}
//
//			@Override
//			public void setKi(float value) {
//				Ki = value;
//			}
//
//			@Override
//			public void setKd(float value) {
//				Kd = value;
//			}
//
//			@Override
//			public void setScale(float value) {
//				scale = value;
//			}
//
//			@Override
//			public void report() {
//				super.setOffset(offset);
//				super.setPower(Tp);
//				super.setKp(Kp);
//				super.setKi(Ki);
//				super.setKd(Kd);
//				super.setScale(scale);
//			}
//
//		};
//
	}

	public void run() {
		while (true) {

			car.forward(SensorReadings.powerA, SensorReadings.powerC);
			car.pickUp(70);

				
			if(SensorReadings.colorSideNormalized <= SensorReadings.colorBlackNormalized + 30 ){
				car.stop();
				Sound.beep();

				/*--------Starting brick------------ */
				//Correct to find line again
				while (SensorReadings.lightFront > SensorReadings.black + 75) {
					car.forward(0, 60);
				}
				
				for (int i = 0; i < 3; i++) {
					
					MotorPort.A.resetTachoCount();
					SensorReadings.tachoCountA = 0;

					//drive to block
					while (SensorReadings.tachoCountA < 260) {
						car.forward(SensorReadings.powerA, SensorReadings.powerC);
					}

					car.stop();
					Delay.msDelay(500);

					//drive to intersection
					while(SensorReadings.colorSideNormalized>SensorReadings.colorBlackNormalized+30){
						driveBlind();
					}

					//and a little more
					Delay.msDelay(100);
					car.stop();
					car.stopPickup();
					
					examineBlock();
					/*----- ONE BRICK DOWN ----- */

				}
				
				while (true) {
					car.stop();
				}
			}

		}
	}

	private void examineBlock() {
		
		//let the block pass through and continue
		if (SensorReadings.colorIdUnder == Color.RED) {
			car.pickUp(70);
			return;
		//time to spin the block
		} else if (SensorReadings.colorIdUnder == Color.BLUE) {
			MotorPort.A.resetTachoCount();
			SensorReadings.tachoCountA = 0;

					car.stop();
					car.stopPickup();
					car.noCommand();
					pilot.rotate(180);
					Delay.msDelay(1500);
					
					while(true){
						car.stop();
						car.pickUp(0);
						
					}
				}
				
			//drive for ward for better placement after spin
			while (SensorReadings.tachoCountA < 180) {
				driveBlind();
			}
			car.stop();
			Delay.msDelay(500);
			
			//turn 90 degree
			while (SensorReadings.colorSideNormalized > SensorReadings.colorBlackNormalized + 50) {
				car.forward(70, -70);	
			}
			
			//get past the black line
			car.forward(70, -70);
			Delay.msDelay(200);
			
			//turn another 90 degree
			while (SensorReadings.colorSideNormalized > SensorReadings.colorBlackNormalized + 50) {
				car.forward(70, -70);	
			}
			// a little more for correction
			Delay.msDelay(100);
			car.stop();
			
			//spit out the block and backup to place it in the grey area
			while (SensorReadings.colorIdUnder != Color.BLACK) {
				car.backward(70, 70+3);
				car.spitOut(65);	
			}
			Delay.msDelay(600);
			
			while (true) {
				car.stop();
				car.stopPickup();
			}
		}
		
	private void driveBlind() {
		car.forward(70+3, 70);
	}
}