package space.bots.solar.behaviors;

import space.bots.behaviors.ThreadedBehavior;
import space.bots.solar.data.Colors;
import space.bots.solar.data.SensorReadings;

public class BangBangLineFollowerBehavior extends ThreadedBehavior {

	private static final int SPEED_HIGH = 300;
	private static final int SPEED_LOW = 200;

	public void run() {
		while (true) {
			if (Colors.isBlack(SensorReadings.lightFront))
				bot.forward(SPEED_HIGH, SPEED_LOW);
			else
				bot.forward(SPEED_LOW, SPEED_HIGH);
		}
	}
}