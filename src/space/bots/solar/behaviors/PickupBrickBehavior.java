package space.bots.solar.behaviors;

import lejos.nxt.Motor;
import lejos.robotics.Color;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;
import space.bots.arbitration.SharedSpaceBot;
import space.bots.behaviors.ThreadedBehavior;
import space.bots.solar.data.SensorReadings;

public class PickupBrickBehavior extends ThreadedBehavior {

	private SharedSpaceBot car = new SharedSpaceBot();
	private DifferentialPilot pilot = new DifferentialPilot(7.0, 22.0, Motor.B, Motor.A, true);
	
	public PickupBrickBehavior(SharedSpaceBot car) {
		this.setName("Pickup Behavior");
		this.car = car;
		pilot.setRotateSpeed(50.0);
	}

	public void run() {
		while (true) {
			
			while (SensorReadings.colorIdUnder == Color.BLUE || SensorReadings.colorIdUnder == Color.RED) {
				car.stop();
				Delay.msDelay(150);
				car.stopPickup();
				Delay.msDelay(2000);
				// let it pass straight through. Currently for test
				pilot.rotate(180);
				Delay.msDelay(2000);
			}
			car.noCommand();
		}
	}
}
